import React, { useContext } from "react";
import { GlobalContext, GlobalProvider } from "../context/GlobalState";
import Transaction from "./Transaction";
const TransactionList = () => {
  const { transactions, deleteTransaction } = useContext(GlobalContext);
  console.log(transactions);
  return (
    <GlobalProvider>
      <h3>History</h3>
      <ul className="list">
        {transactions.map((trans) => (
          <Transaction
            key={trans.id}
            transaction={trans}
            onDelete={deleteTransaction}
          />
        ))}
      </ul>
    </GlobalProvider>
  );
};

export default TransactionList;
