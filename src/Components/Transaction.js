import React from "react";
const Transaction = ({ transaction, onDelete }) => {
  const sign = transaction.amount >= 0 ? "+" : "-";
  return (
    <React.Fragment>
      <li className={sign === "+" ? "plus" : "minus"}>
        {transaction.text}{" "}
        <span>
          {sign}${Math.abs(transaction.amount)}
        </span>
        <button
          className="delete-btn"
          onClick={() => {
            onDelete(transaction.id);
          }}
        >
          x
        </button>
      </li>
    </React.Fragment>
  );
};

export default Transaction;
