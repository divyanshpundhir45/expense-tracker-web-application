import React, { useContext } from "react";
import { GlobalContext, GlobalProvider } from "../context/GlobalState";
const IncomeExpenses = () => {
  const { transactions } = useContext(GlobalContext);
  let income = 0,
    expense = 0;
  for (let i = 0; i < transactions.length; i++) {
    if (transactions[i].amount > 0) income += transactions[i].amount;
    else expense += Math.abs(transactions[i].amount);
  }
  return (
    <GlobalProvider>
      <div className="inc-exp-container">
        <div>
          <h4>Income</h4>
          <p className="money plus">${income}</p>
        </div>
        <div>
          <h4>Expense</h4>
          <p className="money minus">${expense}</p>
        </div>
      </div>
    </GlobalProvider>
  );
};

export default IncomeExpenses;
