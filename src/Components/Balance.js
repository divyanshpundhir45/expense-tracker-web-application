import React, { useContext } from "react";
import { GlobalContext, GlobalProvider } from "../context/GlobalState";
const Balance = () => {
  const { transactions } = useContext(GlobalContext);
  let balance = 0;
  for (let i = 0; i < transactions.length; i++)
    balance = balance + transactions[i].amount;
  return (
    <GlobalProvider>
      <h4>Your Balance.</h4>
      <h1>${balance}</h1>
    </GlobalProvider>
  );
};
export default Balance;
